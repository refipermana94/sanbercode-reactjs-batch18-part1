//Soal 1
console.log("---Jawaban Soal 1---")
console.log("---Relese 0---")
class Animal {
    constructor(name){
        this.name= name;
        this.legs= 4;
        this.cold_blooded="false";
    }
    get Animalname() {
        return this.Animalname;
    }
    set Animalname(x) {
        this.Animalname = x;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("---Relese 1---")

class Frog extends Animal{
    constructor(name){
        super(name);
    }
    jump() {
        return "hop hop";
    }

}

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
kodok.legs = 2;
kodok.cold_blooded = 'true';
console.log(kodok)

class Ape extends Animal{
    constructor(name){
        super(name);
    }
    yell() {
        return "Auoo";
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo
sungokong.legs = 2;
sungokong.cold_blooded = 'true';
console.log(sungokong)

//Soal 2
console.log("---Jawaban Soal 2---")

class Clock{
    constructor({ template }){
        this.template = template
        this.timer

    }
    render(){
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
        console.log(output);

    }
    stop () {
        clearInterval(timer);
    };

    start (){
        this.render()
        this.timer = setInterval(()=>this.render(), 1000);
    }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start(); 

