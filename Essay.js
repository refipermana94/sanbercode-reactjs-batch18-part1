console.log("jawaban No 11")
function filterBooksPromise(colorful, amountOfPage) {
    return new Promise(function(resolve, reject){
        var books = [
            {name: "shinchan", totalPage:50, isColorful:true},
            {name: "Kalkulus", totalPage:250, isColorful:false},
            {name: "doraemon", totalPage:40, isColorful:true},
            {name: "algoritma", totalPage:300, isColorful:false},
            ]
        if (amountOfPage > 0){
            resolve(books.filter(x=> x.totalPage >= amountOfPage && x.isColorful == colorful));
        
        }else{
            var reason = new Error("maaf parameter salah")
            reject(reason);
        }
    });
}
filterBooksPromise(500, books[0], item1 =>{
    filterBooksPromise(item1, books[1], item2 =>{
      filterBooksPromise(item2, books[2], item3 =>{
           filterBooksPromise(item3, books[3], item4 =>{
             return item4;               
           })
      })
    })
})
console.log("jawaban No 12")
class BangunDatar {
    constructor(name){
        this.name= name;
        this.Luas= 8 * 8;
        this.Keliling= 4 * 8;
        
    }
    get BangunDatarname() {
        return this.BangunDatar;
    }
    set BangunDatar(x) {
        this.BangunDatar = x;
    }
}

var persegi = new BangunDatar("persegi");

console.log(persegi.name) 
console.log("Luas"+"="+persegi.Luas)
console.log("Keliling"+"="+persegi.Keliling)

class lingkaran{
    constructor(name){
        this.name= name;
        this.Luas= (3.14)*4*4;
        this.Keliling= 2 *(3.14) * 4;
        
    }
    get lingkaranname() {
        return this.lingkaranname;
    }
    set lingkaranname(x) {
        this.lingkaranname = x;
    }
}

var lk = new lingkaran("lingkaran");

console.log("jawaban No 13")

console.log(lk.name) 
console.log("Luas"+"="+lk.Luas)
console.log("Keliling"+"="+lk.Keliling)

const data = [{name: "John", age: 25, gender: "Male", profession: "Engineer", 
photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"}, 
{name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"}, 
{name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"}, 
{name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }]

console.log("jawaban No 14")
const bl = function(s) {
    return s*s*s;
}
console.log("Volume Balok"+bl(9)); 
const kb = function(p, l, t) {
    return p * l * t;
}
console.log("Volume Kubus"+kb(29, 12, 8)); 

console.log("jawaban No 15")

let warna= ["biru", "merah","kuning","hijau"]
let dataBukuTambahan ={
    penulis:"jhon doe",
    tahunTerbit:2020
}
let buku ={
    nama:"pemograman dasar",
    jumlahHalaman: 172,
    warnaSampul:["hitam"]
}
warna.splice(1, 0, "hitam")
console.log(warna) 

buku.splice(1, 0, dataBukuTambahan)
console.log(buku) 
