//Soal 1
//luas
console.log("---jawaban soal 1---")
const luasLingkaran =(v, r)=>{
    let hasil= v * (r * r);
    return hasil;
}
console.log(luasLingkaran(3.14 , 14))
//keliling
const kelilingLingkaran = (v, r) => {
    let hasil = 2 * v * r;
    return hasil;
}
console.log(kelilingLingkaran(3.14, 14))

//soal 2
console.log("---jawaban soal 2---")
const nm1 = 'saya'
const nm2= 'adalah'
const nm3 = 'seorang'
const nm4 = 'programer'
const nm5 = 'developer'
const theString = ` ${nm1} \n ${nm2} \n ${nm3} \n ${nm4} \n ${nm5}`
console.log(theString) 

//soal 3
console.log("---jawaban soal 3---")
const newFunction = function literal(firstName, lastName) {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function () {
            console.log(firstName + " " + lastName)
            
        }
    }
}
newFunction("William", "Imoh").fullName()
//soal 4
console.log("---jawaban soal 4---")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation, spell } = newObject
console.log(firstName, lastName, destination, occupation)

//soal 5
console.log("---jawaban soal 5---")
let combinedArray = [["Will", "Chris", "Sam", "Holly"], ["Gill", "Brian", "Noel", "Maggie"]]
console.log(combinedArray)

