//Jwaban Soal 1
var kataPertama ="saya";
var kataKedua ="senang";
var kataKetiga ="belajar";
var kataKeempat ="javascript";
console.log(kataPertama + ' ' + kataKedua + ' ' + kataKetiga + ' ' + kataKeempat.toUpperCase());

// Jawaban Soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
var strInt_1 = parseInt(kataPertama);
var strInt_2 = parseInt(kataKedua);
var strInt_3 = parseFloat(kataKetiga);
var strInt_4 = parseFloat(kataKeempat);
console.log(strInt_1+strInt_2+strInt_3+strInt_4);

//Jawaban Soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 25); 
var kataKelima = kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

//Jawaban soal 4
var nilai ="75";
if (nilai >=80){
    console.log(nilai,"indeksnya A")
}
else if(nilai >= 70 & nilai < 80){
    console.log(nilai, "indeksnya B")
}
else if (nilai >= 60 & nilai < 70){
    console.log(nilai, "indeksnya C")
}
else if (nilai >= 50 & nilai < 60){
    console.log(nilai, "indeksnya D")
}else{
    console.log(nilai, "indeksnya e")
}

//Jawaban soal no 5
var tanggal = 1;
var bulan = 11;
var tahun = 1994;
var strInt_1 = parseInt(bulan);
switch (strInt_1) {
    case 1:{
        console.log(tanggal, 'Januari',tahun);
        break;
    }
    case 2: {
        console.log(tanggal, 'Februari',tahun);
        break;
    }
    case 3: {
        console.log(tanggal, 'Maret',tahun);
        break;
    }
    case 4: {
        console.log(tanggal, 'April',tahun);
        break;
    }
    case 5: {
        console.log(tanggal, 'Mei',tahun);
        break;
    }
    case 6: {
        console.log(tanggal, 'Juni',tahun);
        break;
    }
    case 7: {
        console.log(tanggal, 'Juli',tahun);
        break;
    }
    case 8: {
        console.log(tanggal, 'Agustus',tahun);
        break;
    }
    case 9: {
        console.log(tanggal, 'September',tahun);
        break;
    }
    case 10: {
        console.log(tanggal, 'Oktober',tahun);
        break;
    }
    case 11: {
        console.log(tanggal, 'November', tahun);
        break;
    }
    case 12: {
        console.log(tanggal, 'Desember',tahun);
        break;
    }
}

